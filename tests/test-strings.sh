#%include std/test.sh
#%include std/strings.sh

test_join() {
    local result="$1"; shift
    local joined="$(strings:join "$@")"
    
    echo "$joined"

    [[ "$result" = "$joined" ]] && return 0
    return 1
}

test_split() {
    local idx="$1"; shift
    local func="$1"; shift

    local resarray
    
    "$func" resarray "$@"

    echo "${resarray[$idx]}"
}

test:require test_join "a+b+c" + a b c
test:require test_join "a" + a
test:require test_join "" "" ""
test:require test_join "thing and sung" " and " thing sung
test:forbid test_join "+a+b" + a b

test:matches 'b c' test_split 1 strings:split / "a/b c/d"

test:matches 'b,c' test_split 1 strings:split:escaping "," 'a,b\,c,d'
test:matches 'b\' test_split 1 strings:split:escaping "," 'a,b\\,c,d'
test:matches 'b\,c' test_split 1 strings:split:escaping "," 'a,b\\\,c,d'

$%trap EXIT exit_report() { test:report; }
