# Include this file if your tests depend on TTY functionality/presence

if [[ "${NOSHELL_SKIP_TESTS:-}" = true ]]; then
    echo "Not run in a normal shell. These tests are meaningless -------> skipped."
    exit 0
fi

