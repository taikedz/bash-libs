#%include std/test.sh
#%include std/searchpaths.sh
#%include tmp-tests.sh

mkdir -p tmp-tests/one "tmp-tests/dir two"
touch tmp-tests/one/file1 "tmp-tests/dir two/file2" tmp-tests/one/fileboth "tmp-tests/dir two/fileboth"

paths="tmp-tests/dir two:tmp-tests/one"

test:matches tmp-tests/one/file1 searchpaths:file_from "$paths" file1
test:matches "tmp-tests/dir two/file2" searchpaths:file_from "$paths" file2
test:matches "tmp-tests/dir two/fileboth" searchpaths:file_from "$paths" fileboth
test:matches "" searchpaths:file_from "$paths" fileX

$%trap EXIT exit_report() { test:report; }
