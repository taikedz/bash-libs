#%include std/test.sh
#%include std/crypt.sh
#%include std/out.sh

test:require false

out:warn "This library cannot be reliably tested due to the existence of multiplexed STDIO consumers"
out:error "This test suite will always fail until this can be resolved."

$%trap EXIT exit_report() { test:report; }
