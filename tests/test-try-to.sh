#%include std/test.sh
#%include std/try-to.sh

giveline() {
    # Print one line of output on each attempt
    echo x
    "$@"
}

# Wrappers, so that these can be single-word commands
giveline:false() { giveline false; }
giveline:true() { giveline true; }

try_to_count() {
    local command="$1"; shift
    local reply_sequence="$1"; shift

    echo "$reply_sequence"|sed 's/,/\n/g'|try-to "$command" 2>/dev/null|grep '^' -c
}


test:matches 1 try_to_count giveline:false n
test:matches 2 try_to_count giveline:false y,n
test:matches 2 try_to_count giveline:false y,n,y,y
test:matches 5 try_to_count giveline:false y,y,y,y,n
test:matches 1 try_to_count giveline:true y,n
test:matches 1 try_to_count giveline:true n,n

$%trap EXIT exit_report() { test:report; }
