#%include std/test.sh
#%include std/args.sh

arguments1=(-m hello --message="this is a long message" -t extra tokens)
arguments2=(-m hello --message "this is a long message" -t extra tokens)

arg_defs=(
    s:short:-m
    s:long:--message
    b:bool:-t
)

test_parse_flagging() {
    local unflagged="$1"; shift
    local short long bool

    args:parse arg_defs "$unflagged" "${arguments2[@]}" 2>&1
}

print_valuename() {
    declare -n p_value="$1"
    echo "${p_value[*]}"
}

parse_extract() {
    local valuename="$1"; shift
    local short long bool unflagged

    args:parse arg_defs unflagged "${arguments2[@]}" 2>&1

    print_valuename "$valuename"
}

parse_extract_unflagged() {
    local indexnum="$1"; shift
    local short long bool unflagged

    args:parse arg_defs unflagged "${arguments2[@]}" 2>&1

    echo "${unflagged[$indexnum]}"
}

test:matches hello args:get -m "${arguments1[@]}"
test:matches "this is a long message" args:get --message "${arguments1[@]}"
test:require args:has -t "${arguments1[@]}"
test:forbid args:has -x "${arguments1[@]}"

test:matches "hello" parse_extract short
test:matches "this is a long message" parse_extract long
test:matches "true" parse_extract bool
test:matches "tokens" parse_extract_unflagged 1

test:require test_parse_flagging ok
test:forbid  test_parse_flagging -

test:matches '"hi there"' args:quote "hi there"
test:matches '""' args:quote ""
test:matches 'one "two three" four' args:quote one "two three" four

$%trap EXIT exit_report() { test:report; }
