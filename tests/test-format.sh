#%include std/format.sh
#%include std/test.sh

do:format() {
    echo "$2" | "format:$1" "$3" | head -n1
}

test:matches 'abc     def' do:format columns $'abc\tdef\n123456\t789' $'\t'
test:matches 'abc     def' do:format columns $'abc,def\n123456,789' ','

test:matches "one " do:format wrap "one two three" 4
test:matches "one two " do:format wrap "one two three" 8

$%trap EXIT exit_report() { test:report; }
