#!/usr/bin/env bash

#%include std/test.sh
#%include std/debug.sh
#%include tmp-tests.sh

get_debug_stream() {
    "$@" 2>&1
}

debugbreak_echovar() {
    echo -e "\$$1\n" | debug:break 2>/dev/null
}

debugbreak_quitfirst() {
    echo -e "quit\n\$$1\n" | debug:break 2>/dev/null
}

debugdump_discardstderr() {
    echo "$1" | debug:dump 2>/dev/null
}

debugdump_discardstderr_hasdebug() {
    # Side note - I used to use the silent '-q' flag with grep here
    # This caused a pipe error, and failed the test, even if everything succeeded
    # https://stackoverflow.com/questions/19120263/why-exit-code-141-with-grep-q
    #
    ## "This is because grep -q exits immediately with a zero status as soon as a
    ## match is found. The zfs command is still writing to the pipe, but there is
    ## no reader (because grep has exited), so it is sent a SIGPIPE signal from
    ## the kernel and it exits with a status of 141."
    echo "$1" | debug:dump 2>&1|grep 'DEBUG:'
}

breakdata="my data"

out:info "Testing false mode"
DEBUG_mode=false

test:matches "" get_debug_stream debug:print "hello"
test:matches "" debugbreak_echovar "breakdata"
test:require debugbreak_quitfirst "breakdata"

# Test that debug's modifications don't get into the actual data stream
test:matches "Data" debugdump_discardstderr "Data"
test:forbid  debugdump_discardstderr_hasdebug "Data"



out:info "Testing true mode"
DEBUG_mode=true

test:matches "${CBBLU}DEBUG: ${CBLU}hello${CDEF}" get_debug_stream debug:print "hello"
test:matches "$breakdata" debugbreak_echovar "breakdata"
test:forbid  debugbreak_quitfirst "breakdata"

test:matches "Data" debugdump_discardstderr "Data"
test:require debugdump_discardstderr_hasdebug "Data"

$%trap EXIT exit_report() { test:report; }

