# Include tmp-tests.sh in your test file
# to make tmp-tests available to your test run
# it will clean itself on its own

tmpdir=tmp-tests
mkdir -p "$tmpdir"
$%trap EXIT tmrests_exit() { rm -r tmp-tests; }
