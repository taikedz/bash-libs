#!/bin/bash

#%include std/arrays.sh
#%include std/test.sh

arrays_get() {
    # Ensure unpacking is done as expected
    # Don't rely on test:match with 'echo' for more complex data because of
    #  the whitespace issue with 'echo'
    local var res
    res=0
    arrays:unpack var "$@" || res="$?"

    if [[ "$res" = 0 ]]; then
        echo "$var"
    fi

    return "$res"
}

# ====

AR_main=(one two "three four" five $'\t' $'\n' $'\r\n' $'\r' $'a\nb\r\ndata\n')
AR_serialized=($(arrays:serialize "${AR_main[@]}")) # Relies on space-splitting!!
arrays:deserialize AR_deserialized "${AR_serialized[@]}"

test:matches 9 echo "${#AR_serialized[@]}"

test:matches one arrays_get 0 "${AR_serialized[@]}"
test:matches "three four" arrays_get 2 "${AR_serialized[@]}"

test:require arrays_get 1 "${AR_serialized[@]}"
test:forbid arrays_get 10 "${AR_serialized[@]}"
test:forbid arrays_get -1 "${AR_serialized[@]}"

for i in {4..8}; do
    # expect original (main) , command echo (deserialized)
    # box to prevent whitespace loss
    test:matches "[${AR_main[$i]}]" echo "[${AR_deserialized[$i]}]"
done

$%trap EXIT exit_report() { test:report; }
