#%include std/test.sh
#%include std/log.sh
#%include std/strings.sh
#%include std/args.sh

#%include tmp-tests.sh

testdir=tmp-tests
mkdir -p "$testdir"
$%trap EXIT onexit() { rm -r "$testdir"; }

logfile="$testdir/test.log"

$%function log_write(uselevel logcmd message) {
    local res=0
    log:use_file "$logfile"
    log:level "$uselevel"

    "$logcmd" "$message"

    grep -oP "$message$" "$logfile" || res="$?"
    rm "$logfile"
    return "$res"
}

$%function log_dump_getmessage(uselevel message) {
    local res=0
    log:use_file "$logfile"
    log:level "$uselevel"

    echo "$message" | log:dump >/dev/null

    grep -oP "$message$" "$logfile" || res="$?"
    rm "$logfile"
    return "$res"
}

$%function log_dump_getdata(uselevel message) {
    local res=0
    log:use_file "$logfile"
    log:level "$uselevel"

    echo "$message" | log:stream "$uselevel" > "datafile"

    grep -oP "$message$" "datafile" || res="$?"
    rm "$logfile" "datafile"
    return "$res"
}

test:matches "fail written to fail" log_write fail log:fail "fail written to fail"
test:matches "" log_write fail log:warn "warn written to fail"
test:matches "" log_write fail log:info "info written to fail"
test:matches "" log_write fail log:debug "debug written to fail"

test:matches "fail written to warn" log_write warn log:fail "fail written to warn"
test:matches "warn written to warn" log_write warn log:warn "warn written to warn"
test:matches "" log_write warn log:info "info written to warn"
test:matches "" log_write warn log:debug "debug written to warn"

test:matches "fail written to info" log_write info log:fail "fail written to info"
test:matches "warn written to info" log_write info log:warn "warn written to info"
test:matches "info written to info" log_write info log:info "info written to info"
test:matches "" log_write info log:debug "debug written to info"

test:matches "fail written to debug" log_write debug log:fail "fail written to debug"
test:matches "warn written to debug" log_write debug log:warn "warn written to debug"
test:matches "info written to debug" log_write debug log:info "info written to debug"
test:matches "debug written to debug" log_write debug log:debug "debug written to debug"

test:matches "" log_dump_getmessage fail "data"
test:matches "" log_dump_getmessage info "data"
test:matches "" log_dump_getmessage warn "data"
test:matches "data" log_dump_getmessage debug "data"

test:matches "data" log_dump_getdata fail "data"
test:matches "data" log_dump_getdata info "data"
test:matches "data" log_dump_getdata warn "data"
test:matches "data" log_dump_getdata debug "data"

$%trap EXIT exit_report() { test:report; }
