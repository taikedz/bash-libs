#%include std/test.sh
#%include std/safe.sh
#%include tmp-tests.sh

touch tmp-tests/safe1 tmp-tests/safe2 tmp-tests/safe3

canglob() {
    local x
    for x in tmp-tests/safe*; do
        if [[ "$x" = 'tmp-tests/safe*' ]]; then
            return 1
        else
            return 0
        fi
    done
}

canspacesplit() {
    local arr=($(echo one two three))

    [[ "${#arr[@]}" = 3 ]]
}

# A dummy function that calls an undefined variable
#  and a check function that tries it whilst silencing output

fail_var() { echo "$undefined"; }
novar_isok() { fail_var &>/dev/null ; }

# Wrap the false-check in an external script
# since any other way of checking puts it in
# a conditional, canceling out the effect of 'set -e'

false_isok() {
echo "#%include std/safe.sh" > tmp-tests/false.sh
cat << EOF >> tmp-tests/false.sh
false
echo OK
EOF

"$BBEXEC" tmp-tests/false.sh tmp-tests/false
tmp-tests/false | grep -q OK
}

# =====

echo "Activate space split and glob"

safe:glob on
safe:space-split on

test:require canglob
test:require canspacesplit

echo "Deactivate space split and glob"

safe:glob off
safe:space-split off

test:forbid canglob
test:forbid canspacesplit

test:forbid novar_isok
test:forbid false_isok

$%trap EXIT exit_report() { test:report; }
