#%include std/test.sh
#%include std/varify.sh

baddata="Collapsed)-=*variable"

test:matches Collapsed____variable varify:var "$baddata"
test:matches Collapsed_-__variable varify:fil "$baddata"

$%trap EXIT exit_report() { test:report; }
