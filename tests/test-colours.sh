#%include std/test.sh
#%include std/colours.sh
#%include std/tty.sh

#%include skip-tty-tests.sh

# Required for the pipe test
set -o pipefail

has_colour() {
    "$@"

    tty:is_pipe && echo "${CBRED}pipe detected${CDEF}"

    [[ -n "$CDEF" ]]
}

colorize() {
    echo -n "$1" | colours:pipe "${2:-}" "${3:-}"
}

pipe_has_colour() {
    has_colour colours:auto | :
}

test:forbid has_colour colours:check --color=never

test:require has_colour colours:check --color=always

colours:auto
test:require has_colour : "colours:auto set before test"

test:forbid pipe_has_colour

test:require has_colour : "retained outside of pipe"

test:matches "[31mRED[0m" colorize RED 31
test:matches "[31mREDYELLOW[1;33m" colorize REDYELLOW 31 "1;33"

# The previous test leaves colour at yellow
echo -e "\033[0m"

$%trap EXIT exit_report() { test:report; }
