#%include std/test.sh
#%include std/isroot.sh
#%include std/out.sh

TEST__UID=0 test:require isroot
TEST__UID=1 test:forbid isroot

$%trap EXIT exit_report() { test:report; }
