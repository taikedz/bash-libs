#!/bin/bash

#%include std/tty.sh
#%include std/test.sh

#%include skip-tty-tests.sh


# `detect_pipe` and `test_ispipe`
# because of the way test.sh runs the tests (in a substitution shell, which counts as a pipe)
#   we need to run it outside of the test, and submit only the result itself to the test

detect_pipe() {
    INPIPE=0
    tty:is_pipe || INPIPE="$?"
}

test_ispipe() {
    if [[ -n "${INPIPE:-}" ]]; then
        return "$INPIPE"
    else
        echo "detect_pipe was not run separately" >&2
        return 127
    fi
}

test_ispipe_withinpipe() {
    (
        detect_pipe
        test_ispipe
    )|:
}

test_ispipe_withoutpipe() {
    detect_pipe # Must be done outside of test:* because inside $() counts as a pipe!
    test:forbid test_ispipe
}

# +++

test_ispipe_withoutpipe
test:require test_ispipe_withinpipe

SSH_CLIENT=ok       test:require tty:is_ssh
SSH_CONNECTION=ok   test:require tty:is_ssh
SSH_TTY=ok          test:require tty:is_ssh

SSHTTY= SSH_CLIENT= SSH_CONNECTION= \
                    test:forbid tty:is_ssh

TMUX=stuff          test:require tty:is_multiplexer
TERM=screen         test:require tty:is_multiplexer

$%trap EXIT exit_report() { test:report; }
