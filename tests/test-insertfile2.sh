#%include std/test.sh
#%include std/insertfile2.sh
#%include tmp-tests.sh

echo -e "1 one\n1 two\n1 three" > "$tmpdir/file1"
echo "target-line" > "$tmpdir/file2"

test_insert() {
    local line="$1"; shift
    local check="$1"; shift

    [[ $(insertfile $line "$tmpdir/file1" "$tmpdir/file2" | grep "target-line" -n | cut -d: -f1) = $check ]]
}

test:require test_insert 0 1
test:forbid test_insert 0 2
test:require test_insert 1 2
test:forbid test_insert 1 1
test:forbid test_insert 1 3

$%trap EXIT exit_report() { test:report; }
