#%include std/test.sh
#%include std/runmain.sh
#%include std/out.sh
#%include tmp-tests.sh

# Include statment cannot be in heredoc, as it gets removed when this current script
# is itself compiled
echo "#%include std/runmain.sh" > tmp-tests/work.sh
cat <<EOF >> tmp-tests/work.sh

main() { echo yes; }

runmain work main
EOF

_build() { "$BBEXEC" "$@" 2>&1; }

test:require _build tmp-tests/work.sh tmp-tests/work
test:matches yes tmp-tests/work

test:require _build tmp-tests/work.sh tmp-tests/nope
test:matches "" tmp-tests/nope

$%trap EXIT exit_report() { test:report; }
