#%include std/test.sh
#%include std/osid.sh
#%include tmp-tests.sh

old_osidfiles=("${OSID_IDFILES[@]}")

OSID_IDFILES=(tmp-tests/lsb-release)
cat << EOF > tmp-tests/lsb-release
DISTRIB_ID=old-id
DISTRIB_RELEASE=old-release
DISTRIB_DESCRIPTION=old-description
EOF

test:matches "old-id" osid:name
test:matches "old-release" osid:version
test:matches "old-id" osid:fullname
test:matches "old-description" osid:nameversion

OSID_IDFILES=(tmp-tests/os-release)
cat << EOF > tmp-tests/os-release
ID=new-id
VERSION_ID=new-version
NAME=new-name
PRETTY_NAME=new-pretty
EOF

test:matches "new-id" osid:name
test:matches "new-version" osid:version
test:matches "new-name" osid:fullname
test:matches "new-pretty" osid:nameversion

    # :-)
    OSID_IDFILES=("${old_osidfiles[@]}")
    echo -e "\nAnd so, you are on $(osid:nameversion)\n"

$%trap EXIT exit_report() { test:report; }
