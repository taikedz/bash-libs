#%include std/test.sh
#%include std/vars.sh

V_a=1
V_b=2

print_result() {
    local result
    vars:require result "$@"
    echo "${result[*]}"
}

test:require vars:require result V_a V_b
test:forbid vars:require result V_c
test:matches "V_d V_c" print_result V_a V_b V_d V_c

$%trap EXIT exit_report() { test:report; }
