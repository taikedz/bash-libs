#%include std/test.sh
#%include std/readkv.sh
#%include std/out.sh

test:require false

out:warn "This library is deprecated and will soon be removed."
out:error "It will remain untested."

$%trap EXIT exit_report() { test:report; }
