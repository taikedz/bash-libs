#!/usr/bin/env bash

#%include std/config.sh
#%include std/test.sh

tfile() {
    local configname="$1"; shift
    declare -n tfile="$configname"
    tfile="$(mktemp /tmp/$configname.XXXX)"

    echo -e "$*" > "$tfile"
}

check-value-of() {
    declare -n p_conf="$1"
    echo "${p_conf:-}"
    [[ "${p_conf:-}" = "$2" ]]
}

tfile config_main "first=alpha\nsecond=beta\nthird=gamma\nmatch=val1"
tfile config_sub " second = two\nthird=\n# woof=meow\nsub_match=val2"

config:declare myconfs "$config_main" "$config_sub"
config:load myconfs

test:matches "alpha" echo "$myconfs_first"
test:matches " two" echo "$myconfs_second"
test:matches "" echo "$myconfs_third"

test:matches "alpha" config:read myconfs first
test:matches " two" config:read myconfs second
test:matches "" config:read myconfs third

test:matches "val1" config:read myconfs match
test:matches "val2" config:read myconfs sub_match

test:forbid  config:read myconfs fourth
test:matches "defval" config:read myconfs fourth "defval"

test:require check-value-of myconfs_first alpha
test:require check-value-of myconfs_third ""
test:forbid  config:read myconfs_bad anykey 2>/dev/null # FIXME rather than ignore error output, establish proper codes

test:forbid  config:read myconfs woof

$%trap EXIT exit_report() { test:report; }
