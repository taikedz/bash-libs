#!/bin/bash

#%include std/test.sh
#%include std/syntax-extensions.sh
#%include tmp-tests.sh

$%function callme1(myvar) {
    echo "${myvar:-}"
}

$%function callme2(one ?two) {
    echo "$one $two"
}

$%function build_function(fstring) {
    local src=tmp-tests/src.sh
    local dest=tmp-tests/dest.sh

    echo "$dest"

    echo "$fstring" > "$src"
    "${BBEXEC:-}" "$src" "$dest"
}

$%function run_function(fstring) {
    local scr
    scr="$(build_function "$fstring" 2>/dev/null)" || {
        echo "--- INVALID BUILD ---"
        return "$?"
    }

    "$scr" "$@" 2>&1
}

s_se=$'#%include std/syntax-extensions.sh\nset -eu\n'
s_callme1="${s_se}"'$%function callme1(myvar) { echo "$myvar"; };  callme1 "$@"'
s_callme2="${s_se}"'$%function callme2(one ?two) { echo "$one $two"; }; callme2 "$@"'
s_bad1="${s_se}"'$%function thing(?optional positional) { :; }; thing'
s_bad2="${s_se}"'$%function thing(?optional *positional) { :; }; thing'

test:matches "simple string" run_function "$s_callme1" "simple string"
test:matches "a "   run_function "$s_callme2" "a"
test:matches "a b" run_function "$s_callme2" a b

test:forbid run_function "$s_bad1"
test:forbid run_function "$s_bad2"

$%trap EXIT exit_report() { test:report; }
