#%include std/test.sh
#%include std/cdiff.sh

test_colorize() {
    echo "$*" |cdiff:colorize
}

omit_count() {
    grep -Ev '^___ ' >&2 
}

extract_count() {
    grep -E '^___ ' | sed -r 's/^___ //' | cut -d: -f"$1"
}

rantests="$(
    tty:ispipe() { false; }
    colours:check --color=always

    test:matches "[1;31m-Hello[0m" test_colorize -Hello
    test:matches "[1;32m+Bye[0m" test_colorize +Bye
    test:matches "[1;36m---Blue[0m" test_colorize ---Blue
    test:matches "[1;36m+++Blue[0m" test_colorize +++Blue

    echo "___ $TEST_testsran:$TEST_testfailurecount"
)"

echo "$rantests" | omit_count
tr_a="$(echo "$rantests"|extract_count 1)"
tf_a="$(echo "$rantests"|extract_count 2)"

rantests="$(
    tty:ispipe() { true; }
    colours:check --color=never

    test:matches "-Hello" test_colorize -Hello
    test:matches "+Bye" test_colorize +Bye
    test:matches "---Blue" test_colorize ---Blue
    test:matches "+++Blue" test_colorize +++Blue

    echo "___ $TEST_testsran:$TEST_testfailurecount"
)"

echo "$rantests" | omit_count
tr_b="$(echo "$rantests"|extract_count 1)"
tf_b="$(echo "$rantests"|extract_count 2)"

TEST_testsran=$((tr_a + tr_b))
TEST_testfailurecount=$((tf_a + tf_b))

$%trap EXIT exit_report() { test:report; }
