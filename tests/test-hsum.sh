#!/usr/bin/env bash

#%include std/hsum.sh
#%include std/test.sh

sumup() {
    local base="$1"; shift

    echo "$*"|sed -r 's/\s+/\n/g' | hsum:sum "$base"
}

test:matches "3.0KB" sumup 1000 "1K 2K"
test:matches "3.0KB" sumup 1000 "1000 2K"

test:matches "3.0KB" sumup 1024 "1K 2K"
test:matches "4.1MB" sumup 1024 "1K 4M"

test:matches "2.1000KB" sumup 1024 "1000 2K"
test:matches "4.900MB" sumup 1024 "900K 4M"

test:matches "3.0KB" sumup 1000 "1.5K 1.5K"
test:matches "8.3MB" sumup 1000 "1.5K 1.5K 5M 3M"

$%trap EXIT exit_report() { test:report; }
