#%include std/test.sh
#%include std/patterns.sh

match() {
    declare -n p_pattern="$1"; shift
    local string="$1"; shift

    [[ "$string" =~ $p_pattern ]]
}

test:require match PAT_blank ""
test:require match PAT_blank "  "
test:require match PAT_blank $'\t'

test:require match PAT_comment '# stuff'
test:require match PAT_comment '  # spaced stuff'
test:require match PAT_comment $'\t\t# tabbed stuff'
test:forbid  match PAT_comment 'data # not a comment line'

test:require match PAT_num '2'
test:require match PAT_num '139487'
test:forbid  match PAT_num '12347k'

test:require match PAT_cvar 'a'
test:require match PAT_cvar 'abc0'
test:require match PAT_cvar 'abc'
test:require match PAT_cvar '_abc'
test:forbid  match PAT_cvar '0abc'

test:require match PAT_filename 'thing'
test:require match PAT_filename 'thing.skru'
test:require match PAT_filename '.thing.skru'
test:require match PAT_filename 'thing skru'
test:forbid  match PAT_filename 'thing:skru'
test:forbid  match PAT_filename $'a\tb'

test:require match PAT_email 'me@home.place'
test:require match PAT_email 'me+you@home.place'
test:forbid  match PAT_email 'me@home.0'
test:forbid  match PAT_email '@home.com'
test:forbid  match PAT_email 'me@'
test:forbid  match PAT_email 'me@home'
test:forbid  match PAT_email 'me home.0'
test:forbid  match PAT_email 'me'

$%trap EXIT exit_report() { test:report; }
