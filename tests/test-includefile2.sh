#%include std/test.sh
#%include std/includefile2.sh
#%include tmp-tests.sh

echo "#%include file2" > "$tmpdir"/file1
echo "-- separator ----------" >> "$tmpdir"/file1
echo "#%include file2" >> "$tmpdir"/file1
echo "This is file2"> "$tmpdir"/file2

INCLUDEFILE_paths="$tmpdir" INCLUDEFILE_token='#%include' includefile:process "$tmpdir"/file1 > "$tmpdir/outfile"

test:matches 1 grep -c "This is file2" "$tmpdir/outfile"

$%trap EXIT exit_report() { test:report; }
