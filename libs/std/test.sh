#%include std/safe.sh
#%include std/out.sh
#%include std/this.sh
#%include std/args.sh

##bash-libs: test.sh @ %COMMITHASH%

### Tests library Usage:bbuild
#
# Library for testing functions
#
# Example:
#
# 	isgreater() {
# 		echo "Checking $1 > $2"
# 		[[ "$1" -gt "$2" ]]
# 	}
#
# 	test:require isgreater 5 3
# 	test:forbid isgreater 3 5
# 	test:forbid isgreater 5 5
#
# 	test:report
#
###/doc

TEST_testfailurecount=0
TEST_testsran=0

test:ok() {
    echo "${CBGRN} OK ${CDEF} $*"
    TEST_testsran=$((TEST_testsran+1))
}

test:fail() {
    echo "${CBRED}FAIL${CDEF} $*"
    TEST_testsran=$((TEST_testsran+1))
    TEST_testfailurecount=$((TEST_testfailurecount+1))
}

test:_hd_dump() {
    if [[ -n "$1" ]]; then
        echo -n "$1"|hd
    else
        echo "${CBYEL}(no data)${CDEF}"
    fi
}

### test:require COMMAND ARGS ... Usage:bbuild
#
# Runs the command with arguments; if the command returns 0 then success
#
# If the command returns non-zero, then a failure is reported, and the
#  command's output is printed
#
###/doc

test:require() {
    local result=:
    local res=0
    result="$("$@")" || res="$?"
    if [[ "$res" = 0 ]] ; then
        test:ok "REQUIRE: [ $(args:quote "$@") ]"
        if [[ "${ECHO_OK:-}" = true ]]; then
            test:_hd_dump "$result"
        fi
    else
        test:fail "REQUIRE: [ $(args:quote "$@") ]"
        test:_hd_dump "$result"
    fi
}

### test:require COMMAND ARGS ... Usage:bbuild
#
# Runs the command with arguments; if the command returns 0 then a failure
#  is reported, and the command's output is printed
#
# If the command returns non-zero, then success
#
###/doc

test:forbid() {
    local result=:
    local res=0
    result="$("$@")" || res="$?"
    if [[ "$res" = 0 ]] ; then
        test:fail "FORBID : [ $(args:quote "$@") ]"
        test:_hd_dump "$result"
    else
        test:ok "FORBID : [ $(args:quote "$@") ]"
        if [[ "${ECHO_OK:-}" = true ]]; then
            test:_hd_dump "$result"
        fi
    fi
}

### test:report Usage:bbuild
#
# Report the number of tests ran, and failed
#
##/doc

test:report() {
    local reportcmd=out:info
    if [[ "$TEST_testsran" -lt 1 ]]; then
        reportcmd=out:warn
        TEST_testfailurecount=1

    elif [[ "$TEST_testfailurecount" -gt 0 ]]; then
        reportcmd=out:error
    fi

    "$reportcmd" "[$THIS_basename] --- Ran $TEST_testsran tests with $TEST_testfailurecount failures"

    return "$TEST_testfailurecount"
}

### test:output EXPECTED COMMAND ... Usage:bbuild
# Test a command's output for expected content.
#
# Runs COMMAND ... and captures output. If the output matches the EXPECTED value, returns 0
# else returns 1
#
# Always prints the output ; combine with test:require and test:forbid
#
# *DEPRECATED* Use test:matches or test:differs instead.
#
###/doc
test:output() {
    out:warn "--- test:output is deprecated | use test:matches or test:differs ---"
    local expect="$1"; shift
    local output

    output="$("$@")" || :

    echo "$output"
    [[ "$expect" = "$output" ]]
}

### test:matches EXPECTED COMMAND ... Usage:bbuild
#
# Test a command's output for expected content.
#
# Runs COMMAND ... and captures output. If the output matches the EXPECTED value, registers a test success;
# else registers a test failure and prints the two results.
#
# Supercedes 'test:require test:output EXPECTED COMMAND ...'
#
###/doc
test:matches() {
    local expect="$1"; shift
    local output quoted

    output="$("$@")" || :
    quoted="$(args:quote "$@")"

    if [[ "$expect" = "$output" ]]; then
        test:ok "MATCHES: [ $quoted ]"
    else
        test:fail "MATCHES: [ $quoted ]"

        out:error "Expected:"
        test:_hd_dump "$expect"

        out:error "But got:"
        test:_hd_dump "$output"

        out:error "++++"
    fi
}

if ! which hd &>/dev/null; then
    out:fail "You need the 'hd' command for hexdump to use this test reporter."
fi
