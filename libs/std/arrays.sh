#%include std/syntax-extensions.sh
#%include std/out.sh

##bash-libs: arrays.sh @ %COMMITHASH%

### Arrays Library Usage:bbuild
#
# Lib for handling array serialization
#
# In Bash, arrays are built by splitting along whitespace.
#  This makes it impossible for external commands to return array
#  data whose tokens contain whitespace, or even empty-string-tokens.
#
# It is also impossible to pass environment variable arrays down to
#  sub processes and subshells.
#
# This library provides a mechanism of "serializing" an array's values to
#  get around the aforementioned limitations.
#
# Recipients of the serialized array must use the deserialization
#  function on each token to retrive its original content.
#
# EXAMPLES
#
# Set an environment configuration with a user and their obfuscated email address.
#
#   MAIN_USER="$(arrays:serialize "Jay Smith" "jsmith at example dot com")"
#
# This variable can be passed down into a process that can deserialize.
#
#
# Such a process might have these lines in it
#
#   # Deserialize environment array
#   #  relies on space-splitting to split the string into tokens
#   arrays:deserialize MAIN_USER $MAIN_USER
#
#   echo "${MAIN_USER[0]} can be contacted at '${MAIN_USER[1]}'"
#
#
###/doc

### arrays:unpack RETURN { IDX TOKENS ... | TOKEN } Usage:bbuild
#
# Deserialize the IDX'th item in TOKENS, and place it in the return variable RETURN
#
# or
#
# Deserialize TOKEN, and place it in the return variable RETURN
#
###/doc
$%function arrays:unpack(*p_returnvar) {
    local idx inner_array rawdata
    idx="$1"; shift

    if [[ -n "$*" ]]; then # Extract from serialized array
        inner_array=("$@")
        [[ $idx -lt ${#inner_array[@]} ]] || return 1
        [[ $idx -ge 0 ]] || return 1
        arrays:_deserialize_token_raw rawdata "${inner_array[$idx]}"

    else # No additional arguments passed; presume idx is in fact a serialized token
        arrays:_deserialize_token_raw rawdata "$idx"
    fi

    p_returnvar="$rawdata"
}

### arrays:get { IDX TOKENS ... | TOKEN } Usage:bbuild
#
# *DEPRECATED* Use arrays:unpack instead. arrays:get uses 'echo' to return data, which can cause loss of embedded newlines.
#
# Deserialize the IDX'th item in TOKENS to stdout
#
# or
#
# Deserialize TOKEN to stdout
#
###/doc
arrays:get() {
    out:warn "arrays:get is deprecated"
    local rawget
    arrays:unpack rawget "$@"
    echo "$rawget"
}

### arrays:deserialize ARRAYNAME TOKENS ... Usage:bbuild
#
# Deserialize all tokens into the referenced array.
#
# Note that ARRAYNAME is a string of the array name, not the
#   array values themselves.
#
###/doc
$%function arrays:deserialize(*p_return_array) {
    local token rawdata
    for token in "$@"; do
        arrays:_deserialize_token_raw rawdata "$token"
        p_return_array+=("$rawdata")
    done
}

### arrays:serialize ARGUMENTS ... Usage:bbuild
# 
# Given an array of arguments, return them as a space-separated list
#   of serialized tokens.
#
###/doc
arrays:serialize() {
    local sdata x
    sdata=''

    for x in "$@"; do
        # Box the data in to preserve newliens on end of data
        sdata="$sdata $(echo -n "[$x]"|base64 -w0)"
    done

    echo "$sdata"
}

# Internal - see arrays:get for API use
#
# Deserialize a single token
$%function arrays:_deserialize_token_raw(*p_returnvar) {
    local boxeddata
    boxeddata="$(echo "$1" | base64 -d)"
    p_returnvar="${boxeddata:1:-1}"
}
