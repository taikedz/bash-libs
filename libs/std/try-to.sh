#%include std/colours.sh
#%include std/askuser.sh

##bash-libs: try-to.sh @ %COMMITHASH%

### try-to COMMAND ... Usage:bbuild
# Try to run something, offer the user an opportunity to resolve problems and retry
###/doc

try-to() {
    echo "${CBRED}$*${CDEF}" >&2
    while ! "$@"; do
        if ! askuser:confirm "Retry (y) or continue (n)? (Ctrl-C to abort script)"; then
            return 1
        fi
    done
}

