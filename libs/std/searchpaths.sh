#%include std/out.sh

##bash-libs: searchpaths.sh @ %COMMITHASH%

### searchpaths:file_from PATHDEF FILE Usage:bbuild
#
# Locate a file along a search path.
#
# EXAMPLE
#
# The following will look for each of the files
#  in order of preference of a local lib directory, a profile-wide one, then a system-
#  wide one.
#
#    MYPATH="./lib:$HOME/.local/lib:/usr/local/lib"
# 	searchpaths:file_from "$MYPATH" file
#
# Echoes the path of the first file found.
#
# Returns 1 on failure to find any file.
#
###/doc

__SEARCHPATHS_SEP="$(echo -e '\x09')" # Tab char

function searchpaths:file_from {
    local PATHS="$1"; shift || :
    local FILE="$1"; shift || :

    local oldIFS="$IFS"
    IFS=$'\t'

    for path in $(echo "$PATHS"|tr ':' "${__SEARCHPATHS_SEP}"); do # FIXME use string splitter
        local fpath="$path/$FILE"
        if [[ -f "$fpath" ]]; then
            echo "$fpath"
            return 0
        fi
    done

    IFS="$oldIFS"
    return 1
}
