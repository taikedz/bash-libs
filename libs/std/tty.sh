##bash-libs: tty.sh @ %COMMITHASH%

### tty.sh Usage:bbuild
# Get information on the current terminal session.
###/doc

### tty:is_ssh Usage:bbuild
# Determine whether the TTY is an SSH session.
#
# WARNING: this only works for an SSH connection still in the "landing" account.
# If the user is switched via 'su' or 'sudo', the environment is lost and the variables used to determine this are blank - by default, indicating being not in an SSH session.
###/doc
tty:is_ssh() {
    [[ -n "${SSH_TTY:-}" ]] || [[ -n "${SSH_CLIENT:-}" ]] || [[ "${SSH_CONNECTION:-}" ]]
}

### tty:is_pipe Usage:bbuild
# Determine if we are running in a pipe.
###/doc
tty:is_pipe() {
    [[ ! -t 1 ]]
}

### tty:is_multiplexer Usage:bbuild
# Determine if we are in a terminal multiplexer (detects 'screen' and 'tmux')
###/doc
tty:is_multiplexer() {
    [[ -n "${TMUX:-}" ]] || [[ "${TERM:-}" = screen ]]
}
