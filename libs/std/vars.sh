#%include std/syntax-extensions.sh

##bash-libs: vars.sh @ %COMMITHASH%

### Vars library Usage:bbuild
#
# Functions for checking variables
#
###/doc

readonly VARS_ERR_not_set=10

### vars:are-set VARNAMES ... Usage:bbuild
#
# Check a list of environment variables such that none are non-empty.
#
# If not all variables are set, returns value $VARS_ERR_not_set
#
# Example:
#
#    if vars:are-set myvarA myvarB; then
#        echo "proceeding"
#    fi
#
###/doc

$%function vars:are-set() {
    local _null
    vars:require _null "$@"
}

### vars:require RETURNVAR VARNAMES ... Usage:bbuild
#
# Check a list of environment variables such that none are non-empty.
#
# If variables are empty/not set, the name is added to the return holder variable, and returns value $VARS_ERR_not_set
#
# Example:
#
#   local missing_vars
#   if ! vars:require missing_vars myvarA myvarB myvarC ; then
#       echo "The following variables are not set: $missing_vars"
#   fi
#
###/doc

$%function vars:require(*p_returnarr) {
    local varname res
    res=0

    for varname in "$@"; do
        if ! vars:_has_value "$varname"; then
            p_returnarr+=("$varname")
            res="$VARS_ERR_not_set"
        fi
    done

    return "$res"
}

$%function vars:_has_value(*_p_varname) {
    [[ -n "${_p_varname:-}" ]]
}
