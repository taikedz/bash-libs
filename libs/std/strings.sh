#%include std/syntax-extensions.sh

##bash-libs: strings.sh @ %COMMITHASH%

### Strings library Usage:bbuild
#
# More advanced string manipulation functions.
#
###/doc

### strings:join JOINER STRINGS ... Usage:bbuild
#
# Join multiple strings, separated by the JOINER string
#
# Write the joined string to stdout
#
###/doc

strings:join() {
    # joiner can be any string
    local joiner="$1"; shift || :

    # so we use an array to collect the token parts
    local destring=(:)

    for token in "$@"; do
        destring[${#destring[@]}]="$joiner"
        destring[${#destring[@]}]="$token"
    done

    local finalstring=""
    # first remove holder token and initial join token
    #   before iterating
    for item in "${destring[@]:2}"; do
        finalstring="${finalstring}${item}"
    done
    echo "$finalstring"
}

### strings:split *RETURN_ARRAY SPLITTER STRING Usage:bbuild
#
# Split a STRING along each instance of SPLITTER
#
# Write the result to the variable in RETURN_ARRAY (pass as name reference)
#
# e.g.
#
#   local my_array
#
#   strings:split my_array ":" "a:b c:d"
#
#   echo "${my_array[1]}" # --> "b c"
#
###/doc

$%function strings:split(*p_returnarray splitter string_to_split) {
    local items=(:)

    while [[ -n "$string_to_split" ]]; do
        if [[ ! "$string_to_split" =~ "${splitter}" ]]; then
            items[${#items[@]}]="$string_to_split"
            break
        fi

        local token="$(echo "$string_to_split"|sed -r "s${splitter}.*$")"
        items+=("$token")
        string_to_split="$(echo "$string_to_split"|sed "s^${token}${splitter}")"
    done

    p_returnarray=("${items[@]:1}")
}

### strings:split:escaping *p__target splitter string_to_split Usage:bbuild
#
# Split a string into tokens along splitter ; returns the result in an array name-referenced by p__target
#
# Multiple splitter characters produce multiple tokens.
#
# Splitter characters escaped by preceding with `\` do not cause a split, and are replaced with literal instances of the splitter.
#
# E.g.
#
#   tokens="-a -b my\ path -c"
#   strings:split:escaping result ' ' "$tokens"
#
# Store the split tokens in `${result[@]}`
#
# The contents of the array will be  `['-a', '-b', 'my path', '-c']`
#
###/doc


$%function strings:split:escaping(*p__target splitter string_to_split) {
    local SP_ESCP=$'\001'
    local SP_REAL=$'\002'
    local ESC_ESC=$'\003'
    local holder=(:)

    string_to_split="${string_to_split//\\\\/$ESC_ESC}"
    string_to_split="${string_to_split//\\$splitter/$SP_ESCP}"
    string_to_split="${string_to_split//$splitter/$SP_REAL}"
    string_to_split="${string_to_split//$SP_ESCP/$splitter}"

    strings:split holder "$SP_REAL" "$string_to_split"

    p__target=(:)
    for token in "${holder[@]}"; do
        p__target+=("${token//$ESC_ESC/\\}")
    done

    p__target=("${p__target[@]:1}")
}
