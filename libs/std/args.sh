#%include std/patterns.sh
#%include std/out.sh
#%include std/strings.sh
#%include std/syntax-extensions.sh

##bash-libs: args.sh @ %COMMITHASH%

### args Usage:bbuild
#
# An arguments handling utility.
#
# `args:has` can be used to determine if an exact token exists in the array of arguments
#
# `args:get` can be used to extract individual arguments from an array of arguments; it assumes any flag is to be followed by the actual sought token.
# It follows the convention that short flags are followed by value token ("-s value", "-l value")
#   but that long flags incorporate their value after an equals sign ("--short=value", "--long=value")
#
# `args:parse` provides a fuller argument parsing utility, detecting some basic types, and optionally alerting to when a an argument was not expected.
# It assumes that both short and long flags are followed separately by their value token ("-s value", "--long value")
#
###/doc

readonly ARGS_ERR_NaN=100
readonly ARGS_ERR_unknown_flag=101
readonly ARGS_ERR_missing_value=102
readonly ARGS_ERR_unknown_token=103

### args:get TOKEN ARGS ... Usage:bbuild
#
# Given a TOKEN, find the argument value
#
# Typically called with the parent's arguments
#
# 	args:get --key "$@"
# 	args:get -k "$@"
#
# If TOKEN is an int, returns the argument at that index (starts at 1, negative numbers count from end backwards)
#
# If TOKEN starts with two dashes ("--"), expect the value to be supplied after an equal sign
#
# 	--token=desired_value
#
# If TOKEN starts with a single dash, and is a letter or a number, expect the value to be the following token
#
# 	-t desired_value
#
# Returns 1 if could not find anything appropriate.
#
###/doc

args:get() {
    local seek="$1"; shift || :

    if [[ "$seek" =~ $PAT_num ]]; then
        local arguments=("$@")

        # Get the index starting at 1
        local n=$((seek-1))
        # but do not affect wrap-arounds
        [[ "$n" -ge 0 ]] || n=$((n+1))

        echo "${arguments[$n]}"

    elif [[ "$seek" =~ ^--.+ ]]; then
        args:_get_long "$seek" "$@"

    elif [[ "$seek" =~ ^-[a-zA-Z0-9]$ ]]; then
        args:_get_short "$seek" "$@"

    else
        return 1
    fi
}

args:_get_short() {
    local token="$1"; shift || :
    while [[ -n "$*" ]]; do
        local item="$1"; shift || :

        if [[ "$item" = "$token" ]]; then
            echo "$1"
            return 0
        fi
    done
    return 1
}

args:_get_long() {
    local token="$1"; shift || :
    local tokenpat="^$token=(.*)$"

    for item in "$@"; do
        if [[ "$item" =~ $tokenpat ]]; then
            echo "${BASH_REMATCH[1]}"
            return 0
        fi
    done
    return 1
}

### args:has TOKEN ARGS ... Usage:bbuild
#
# Determines whether TOKEN is present on its own in ARGS
#
# Typically called with the parent's arguments
#
# 	args:has thing "$@"
#
# Returns 0 on success for example
#
# 	args:has thing "one" "thing" "or" "another"
#
# Returns 1 on failure for example
#
# 	args:has thing "one thing" "or another"
#
# "one thing" is not a valid match for "thing" as a token.
#
###/doc

args:has() {
    local token="$1"; shift || :
    for item in "$@"; do
        if [[ "$token" = "$item" ]]; then
            return 0
        fi
    done
    return 1
}

### args:quote ARGS ... Usage:bbuild
#
# Produce a single string with each argument. If the argument has whitespace, double-quotes are added. If the argument has double-quotes as part of its data, these are escaped.
#
###/doc
args:quote() {
    local argstring arg
    argstring=""

    for arg in "$@"; do
        arg="$(echo -n "$arg" | sed -rz 's/"/\\"/g ; s/\n/\\n/g ; s/\r/\\r/g ; s/\t/\\t/g')"

        if (echo "$arg" | grep -Pq '\s') || [[ -z "$arg" ]]; then
            arg="\"${arg}\""
        fi

        argstring="$argstring $arg"
    done

    echo "${argstring:1}"
}

### args:parse *ARGSDEF *FLAGLESS ARGS ... Usage:bbuild
#
# Iterate over the supplied arguments, using an array of argument/variables definitions.
#
# ARGSDEF :
#
# This is an array of flag definitions. Each definition token comprises of three parts, separated by a colon ':':
#
# * type
# * return name
# * flags
#
# The return name is the literal name of a variable to return values to (by name reference)
#
# There are three defined types:
#
# * 'b' - the name reference is set to the string "true" if a relevant flag is found
# * 's' - the name reference is set to the value of the string passed
# * 'n' - the name reference is set to a numerical string if found
#
# Flags is a comma-separated list of flags that trigger a match.
#
# For 's' and 'n' parameters, it is always expected that the following token provide the value.
# NOTE: this is different from args:get behaviour.
#
# FLAGLESS :
#
# The name of the variable into which to put the tokens that were found, but not attached to flags.
# If the name "-" is passed, causes an error when a flagless token is found.
#
# For example:
#
#   #%include std/args.sh
#
#   docopy() {
#       # Define locally to not cause args:parse to write global variables
#       local args_def usebinary infile outfile bytes
#
#       args_def=(
#           "b:usebinary:-b,--binary"
#           "s:infile:-i,--input"
#           "s:outfile:-o,--output"
#           "n:bytes:-n"
#       )
#
#       # Pass 'args_def' by name reference
#       args:parse args_def - "$@"
#
#       echo "Copying from $infile to $outfile"
#       if [[ "$usebinary" = true ]]; then
#           echo "Using binary mode"
#       fi
#       if [[ -n "${bytes:-}" ]]; then
#           echo "Limiting to $bytes bytes."
#       fi
#   }
#
#   # Prints text
#   docopy --input infile.txt -o outfile.bin -b -n 5
#
#   # Fails (unknown flag '-x')
#   docopy --input infile.txt -o outfile.bin -b -x
#
#
#
# UNKNOWN FLAGS
#
# By default, if args:parse finds a flag that was not declared, it will cause a filure exit. You can set an environment variable `ARGS_allow_unknown_flags=true` to bypass this.
#
###/doc
$%function args:parse(*a_argsdefs ?flagless_returnname) {
    local arg __argdef __type __name __flags __flagless

    if [[ "$flagless_returnname" != "-" ]]; then
        __flagless=(:)
        declare -n p_flaglessreturn="$flagless_returnname"
    fi

    while [[ -n "$*" ]]; do
        arg="$1"; shift

        if [[ "$arg" =~ ^- ]]; then
            if ! args:_get_argdef "$arg" "__argdef" "a_argsdefs" && [[ "${ARGS_allow_unknown_flags:-}" = true ]]; then
                out:fail $ARGS_ERR_unknown_flag "Unknown flag '$arg'"
            fi

            args:_unpack_argdef __type __name __flags "$__argdef"

            declare -n p_pointedvar="$__name"

            if [[ "$__type" = b ]]; then
                p_pointedvar=true
            else
                p_pointedvar="${1:-}"; shift || out:fail $ARGS_ERR_missing_value "Argument parse error - expected value for '$__name' (flag was '$arg')"
            fi

            if [[ "$__type" = n ]]; then
                [[ "$p_pointedvar" =~ $PAT_num ]] || out:fail $ARGS_ERR_NaN "Argument parse error - expected number for '$__name' but got '$p_pointedvar'"
            fi

        elif [[ "$flagless_returnname" != "-" ]]; then
            __flagless+=("$arg")

        else
            out:fail $ARGS_ERR_unknown_token "Unrecognized token: '$arg'"
        fi
    done

    if [[ "$flagless_returnname" != "-" ]]; then
        p_flaglessreturn=("${__flagless[@]:1}")
    fi
}

$%function args:_get_argdef(flag *p_argdef *p_a_argsdefs) {
    local argdef_string flags

    for argdef_string in "${p_a_argsdefs[@]}"; do
        flags="${argdef_string##*:}"
        if [[ "$flags" =~ (^|,)$flag(,|$) ]]; then
            p_argdef="$argdef_string"
            return 0
        fi
    done
    return 1
}

$%function args:_unpack_argdef(*p_type *p_name *p_flags argdef_string) {
    local a_argdef
    strings:split a_argdef : "$argdef_string"
    
    p_type="${a_argdef[0]}"
    p_name="${a_argdef[1]}"
    p_flags="${a_argdef[2]}"

    if [[ ! "$p_type" =~ ^s|b|n$ ]]; then
        out:fail "Invalid type '$p_type' for '$p_name'"
    fi
}
