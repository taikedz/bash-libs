#%include std/syntax-extensions.sh

##bash-libs: osid.sh @ %COMMITHASH%

### OS ID Usage:bbuild
#
# Identify the operating system
#
# Different distros provide information a bit differently.
# This library aims to provide that info in a unified API
#
###/doc

OSID_IDFILES=(/etc/os-release /etc/lsb-release)

$%function osid:_getvalue(idfile keyname) {
(
    . "$idfile"
    value="$(. <(echo "echo \"\${$keyname:-}\""))"
    if [[ -n "$value" ]]; then
        echo "$value"
        return 0
    fi
    return 1
)
}

osid:_read() {
    for idfile in "${OSID_IDFILES[@]}"; do
        for property in "$@"; do
            if osid:_getvalue "$idfile" "$property"; then
                return 0
            fi
        done
    done
    return 1
}

### osid:name Usage:bbuild
# Get the distro's shortname
###/doc
osid:name() {
    osid:_read ID DISTRIB_ID
}


### osid:version Usage:bbuild
# Get the distro's version number
###/doc
osid:version() {
    osid:_read VERSION_ID DISTRIB_RELEASE
}


### osid:fullname Usage:bbuild
# Get the long version name of the distro
###/doc
osid:fullname() {
    osid:_read NAME DISTRIB_ID
}


### osid:nameversion Usage:bbuild
# Get the name-and-version string for the distro
###/doc
osid:nameversion() {
    osid:_read PRETTY_NAME DISTRIB_DESCRIPTION
}
