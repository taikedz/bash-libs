#%include std/out.sh
#%include std/runmain.sh

check-testless() {
    out:info "Lib files with no test file"
    for file in libs/std/*.sh; do
        file="$(basename "$file")"
        [[ -f "tests/test-$file" ]] || echo "$file"
    done

    out:info "Test files with no actual tests"
    for file in tests/test-*.sh; do
        grep -P 'test:.+' "$file"|grep -v 'test:report'|grep -qP "test:.+" || echo "$file"
    done
}

runmain check-testless.sh check-testless "$@"
