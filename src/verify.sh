#!/bin/bash

### Verification script Usage:help
# Build scripts and run unit tests
#
# [norm=true] [runtests=false] [bbflags='-c'] ./verify [LIBFILES ...]
#
# * `norm=true` : do not remove test artifacts
# * `runtests=false` : do not run unit tests, just check that he files build
# * `bbflags='-c'` : run shellcheck (via `bbuild` feature)
#
# Any LIBFILE must be in the form of "./lib/SOMEFILE" , the corresponding test
#  that will be searched for wil then be "./tests/test-SOMEFILE"
###/doc

#%include std/autohelp.sh
#%include std/out.sh
#%include std/this.sh
#%include std/colours.sh
#%include std/runmain.sh

#%include check-testless.sh

set_executable() {
    if [[ -z "${BBEXEC:-}" ]]; then
        export BBEXEC=bbuild
    fi

    if [[ ! -f "$BBEXEC" ]] && ! which "$BBEXEC" >/dev/null 2>/dev/null; then
        out:fail 1 "Cannot use [$BBEXEC] to run builds - no such file or command"
    fi

    out:info "Build using \`$BBEXEC\` command"
}

set_targets() {
    CUSTOMTARGETS=false

    if [[ "$#" -gt 0 ]]; then
        targets=("$@")
        CUSTOMTARGETS=true
    else
        oldIFS="$IFS"
        IFS=$'\n\t'
        targets=( $(grep -vP '^\s*(#.*)?$' tests/targets.config) )
        IFS="$oldIFS"
    fi
}

rmfile() {
    [[ "${norm:-}" = true ]] && return || :

    rm "$@" || :
}

run_unit_tests() {
    [[ "${runtests:-}" = true ]] || return

    local testsfile="$1"; shift
    local testname="$(basename "$testsfile")"
    local testtarget="/tmp/$testname"

    if [[ -f "$testsfile" ]]; then
        "$BBEXEC" "$testsfile" "$testtarget"
        bash ${bashflags:-} "$testtarget" || VER_fails=$((VER_fails+1))

        rmfile "$testtarget"
    else
        VER_fails=$((VER_fails+1))
        out:warn "There is no $testsfile test file."
    fi
}

run_verification() {
    for libscript in "${targets[@]}"; do
        items=$((items+1))
        run_unit_tests "$libscript"
    done
}

main() {

    cd "$THIS_dirname"
    export BBPATH="$PWD/libs"

    items=0
    VER_fails=0
    exit_code=0
    : ${runtests=true}

    autohelp:check "$@"

    set_executable
    set_targets "$@"
    run_verification

    echo -e "\n\n\n"
    local endmsg="Verification --- Built $items items with $VER_fails failures."

    if [[ "$VER_fails" -gt 0 ]]; then
        out:error "$VER_fails" "$endmsg"
        exit_code=1
    else
        out:info "$endmsg"
    fi

    if [[ "$CUSTOMTARGETS" = false ]]; then
        check-testless
    fi
}

$%trap EXIT exit_with_code() {
    local trigger_code="$?"

    if [[ "$trigger_code" != 0 ]]; then
        # before exit_code would have been set - different error
        exit "$trigger_code"
    else
        exit $exit_code
    fi
}

time runmain verify.sh main "$@"
